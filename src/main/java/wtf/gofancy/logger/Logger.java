package wtf.gofancy.logger;

import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.function.Consumer;

public class Logger {

    private final org.slf4j.Logger logger;
    private final Marker marker;

    public Logger(final String name, final String marker) {
        this(LoggerFactory.getLogger(name), marker);
    }

    public Logger(final org.slf4j.Logger logger, final String marker) {
        this.logger = logger;
        this.marker = MarkerFactory.getMarker(marker);
    }

    // ERROR >>
    public void error(final String msg) {
        this.logger.error(this.marker, msg);
    }

    public void error(final String format, final Object arg) {
        this.logger.error(this.marker, format, arg);
    }

    public void error(final String format, final Object arg1, final Object arg2) {
        this.logger.error(this.marker, format, arg1, arg2);
    }

    public void error(final String format, final Object... args) {
        this.logger.error(this.marker, format, args);
    }

    public void error(final String msg, final Throwable t) {
        this.logger.error(this.marker, msg, t);
    }

    public void errorBlock(final String msg) {
        this.errorBlock(DumpStackBehavior.CLOSEST_DUMP, msg);
    }

    public void errorBlock(final DumpStackBehavior dsb, final String msg) {
        Utils.block(msg, dsb, this::error);
    }

    public void errorBlock(final Consumer<StringBuilder> blockBuilder) {
        final StringBuilder builder = new StringBuilder();
        blockBuilder.accept(builder);
        this.errorBlock(builder.toString());
    }

    public void errorBlock(final DumpStackBehavior dsb, final Consumer<StringBuilder> blockBuilder) {
        final StringBuilder builder = new StringBuilder();
        blockBuilder.accept(builder);
        this.errorBlock(dsb, builder.toString());
    }
    // << ERROR

    // WARN >>
    public void warn(final String msg) {
        this.logger.warn(this.marker, msg);
    }

    public void warn(final String format, final Object arg) {
        this.logger.warn(this.marker, format, arg);
    }

    public void warn(final String format, final Object arg1, final Object arg2) {
        this.logger.warn(this.marker, format, arg1, arg2);
    }

    public void warn(final String format, final Object... args) {
        this.logger.warn(this.marker, format, args);
    }

    public void warn(final String msg, final Throwable t) {
        this.logger.warn(this.marker, msg, t);
    }

    public void warnBlock(final String msg) {
        this.warnBlock(DumpStackBehavior.CLOSEST_DUMP, msg);
    }

    public void warnBlock(final DumpStackBehavior dsb, final String msg) {
        Utils.block(msg, dsb, this::warn);
    }

    public void warnBlock(final Consumer<StringBuilder> blockBuilder) {
        final StringBuilder builder = new StringBuilder();
        blockBuilder.accept(builder);
        this.warnBlock(builder.toString());
    }

    public void warnBlock(final DumpStackBehavior dsb, final Consumer<StringBuilder> blockBuilder) {
        final StringBuilder builder = new StringBuilder();
        blockBuilder.accept(builder);
        this.warnBlock(dsb, builder.toString());
    }
    // << WARN

    // INFO >>
    public void info(final String msg) {
        this.logger.info(this.marker, msg);
    }

    public void info(final String format, final Object arg) {
        this.logger.info(this.marker, format, arg);
    }

    public void info(final String format, final Object arg1, final Object arg2) {
        this.logger.info(this.marker, format, arg1, arg2);
    }

    public void info(final String format, final Object... args) {
        this.logger.info(this.marker, format, args);
    }

    public void info(final String msg, final Throwable t) {
        this.logger.info(this.marker, msg, t);
    }

    public void infoBlock(final String msg) {
        this.infoBlock(DumpStackBehavior.CLOSEST_DUMP, msg);
    }

    public void infoBlock(final DumpStackBehavior dsb, final String msg) {
        Utils.block(msg, dsb, this::info);
    }

    public void infoBlock(final Consumer<StringBuilder> blockBuilder) {
        final StringBuilder builder = new StringBuilder();
        blockBuilder.accept(builder);
        this.infoBlock(builder.toString());
    }

    public void infoBlock(final DumpStackBehavior dsb, final Consumer<StringBuilder> blockBuilder) {
        final StringBuilder builder = new StringBuilder();
        blockBuilder.accept(builder);
        this.infoBlock(dsb, builder.toString());
    }
    // << INFO

    // DEBUG >>
    public void debug(final String msg) {
        this.logger.debug(this.marker, msg);
    }

    public void debug(final String format, final Object arg) {
        this.logger.debug(this.marker, format, arg);
    }

    public void debug(final String format, final Object arg1, final Object arg2) {
        this.logger.debug(this.marker, format, arg1, arg2);
    }

    public void debug(final String format, final Object... args) {
        this.logger.debug(this.marker, format, args);
    }

    public void debug(final String msg, final Throwable t) {
        this.logger.debug(this.marker, msg, t);
    }

    public void debugBlock(final String msg) {
        this.debugBlock(DumpStackBehavior.CLOSEST_DUMP, msg);
    }

    public void debugBlock(final DumpStackBehavior dsb, final String msg) {
        Utils.block(msg, dsb, this::debug);
    }

    public void debugBlock(final Consumer<StringBuilder> blockBuilder) {
        final StringBuilder builder = new StringBuilder();
        blockBuilder.accept(builder);
        this.debugBlock(builder.toString());
    }

    public void debugBlock(final DumpStackBehavior dsb, final Consumer<StringBuilder> blockBuilder) {
        final StringBuilder builder = new StringBuilder();
        blockBuilder.accept(builder);
        this.debugBlock(dsb, builder.toString());
    }
    // << DEBUG

    // TRACE >>
    public void trace(final String msg) {
        this.logger.trace(this.marker, msg);
    }

    public void trace(final String format, final Object arg) {
        this.logger.trace(this.marker, format, arg);
    }

    public void trace(final String format, final Object arg1, final Object arg2) {
        this.logger.trace(this.marker, format, arg1, arg2);
    }

    public void trace(final String format, final Object... args) {
        this.logger.trace(this.marker, format, args);
    }

    public void trace(final String msg, final Throwable t) {
        this.logger.trace(this.marker, msg, t);
    }

    public void traceBlock(final String msg) {
        this.traceBlock(DumpStackBehavior.CLOSEST_DUMP, msg);
    }

    public void traceBlock(final DumpStackBehavior dsb, final String msg) {
        Utils.block(msg, dsb, this::trace);
    }

    public void traceBlock(final Consumer<StringBuilder> blockBuilder) {
        final StringBuilder builder = new StringBuilder();
        blockBuilder.accept(builder);
        this.traceBlock(builder.toString());
    }

    public void traceBlock(final DumpStackBehavior dsb, final Consumer<StringBuilder> blockBuilder) {
        final StringBuilder builder = new StringBuilder();
        blockBuilder.accept(builder);
        this.traceBlock(dsb, builder.toString());
    }
    // << TRACE
}
