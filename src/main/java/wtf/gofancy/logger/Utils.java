package wtf.gofancy.logger;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.stream.IntStream;

class Utils {
    static void block(final String msg, final DumpStackBehavior dsb, final Consumer<String> log) {
        final String fullMsg = doReplaces("\n" + msg + getStackTrace(dsb) + "\n");

        if (fullMsg.isEmpty()) return;

        final int maxLength = Arrays.stream(fullMsg.split("\n", -1)).map(String::length).max(Integer::compareTo).get();

        final StringBuilder builder = new StringBuilder();

        builder.append(repeat("*", maxLength + 4)).append("\n");

        Arrays.stream(fullMsg.split("\n", -1)).forEach((it) -> {
            builder.append("* ").append(it).append(repeat(" ", maxLength - it.length())).append(" *\n");
        });

        Arrays.stream(builder.append(repeat("*", maxLength + 4)).toString().split("\n")).forEach(log);
    }

    private static String getStackTrace(final DumpStackBehavior dsb) {
        if (dsb == DumpStackBehavior.DO_NOT_DUMP) return "";

        final StackTraceElement[] stack = new Throwable().fillInStackTrace().getStackTrace();

        final int start = dsb == DumpStackBehavior.COMPLETE_DUMP?
                0 :
                (int) Arrays.stream(stack)
                            .limit(4)
                            .map(StackTraceElement::getClassName)
                            .filter(e -> e.equals(L.class.getName()) ||
                                         e.equals(Logger.class.getName()) ||
                                         e.equals(Utils.class.getName()))
                            .count();
        final int end = Math.min(start + (dsb == DumpStackBehavior.CLOSEST_DUMP? 6 : stack.length), stack.length);

        final StringBuilder builder = new StringBuilder("\n\n");

        IntStream.range(start, end).forEach((it) -> {
            builder.append("at ").append(stack[it].toString());
            if (it != end - 1) builder.append("\n");
        });

        if (dsb == DumpStackBehavior.CLOSEST_DUMP && end != stack.length) {
            builder.append("\n... (rest of stack dump omitted)");
        }

        return builder.toString();
    }

    private static String doReplaces(final String msg) {
        return msg.replace("\t", "    ").replace("\r", "");
    }

    private static String repeat(final String str, final int count) {
        final StringBuilder builder = new StringBuilder();
        for (int i = 0; i < count; i++) {
            builder.append(str);
        }
        return builder.toString();
    }
}
