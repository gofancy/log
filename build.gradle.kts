import java.time.Instant
import java.time.format.DateTimeFormatter

plugins {
    `java-library`
    `maven-publish`
}

group = "wtf.gofancy"
version = "2.1.0"

repositories {
    mavenCentral()
}

dependencies {
    implementation(group = "org.slf4j", name = "slf4j-api", version = "2.0.0-alpha1")
}

publishing {
    publications {
        create<MavenPublication>("logger") {
            from(components["java"])
        }

        repositories {
            maven {
                name = "gitlab"
                url = uri("https://gitlab.com/api/v4/projects/25660282/packages/maven")

                credentials(HttpHeaderCredentials::class) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                }
                authentication {
                    create("header", HttpHeaderAuthentication::class)
                }
            }
        }
    }
}

tasks {
    withType<Jar>() {
        manifest {
            attributes(
                "Name" to "${project.group.toString().replace(".", "/")}/${project.name.toLowerCase().replace(" ", "_")}/",
                "Specification-Title" to "Garden of Fancy Logger",
                "Specification-Version" to project.version,
                "Specification-Vendor" to "Garden of Fancy",
                "Implementation-Title" to "${project.group}.${project.name.toLowerCase().replace(" ", "_")}",
                "Implementation-Version" to project.version,
                "Implementation-Vendor" to "Garden of Fancy",
                "Implementation-Timestamp" to DateTimeFormatter.ISO_INSTANT.format(Instant.now())
            )
        }
    }

    withType<Wrapper> {
        gradleVersion = "7.0"
        distributionType = Wrapper.DistributionType.ALL
    }
}
