# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.1.0] - 2021-07-06

### Added

- additional constructors to wrap already existing loggers

### Fixed

- proper filtering of the logger internal calls (for stack dumps)

## [2.0.2] - 2021-05-04

### Removed

- useless runtime dependency on slf4j-simple

## [2.0.1] - 2021-04-30

See version 2.0.0 for changelog (this only exists to *hopefully* fix the publish task)

## [2.0.0] - 2021-04-30

### Added

- block method which accept a StringBuilder consumer
- `Logger` (not to be confused with `L`)

### Changed

- block methods param order for Kotlin compat
- shortened `L`s method names

### Removed

- all old methods

## [1.1.1] - 2021-04-18

### Fixed

- block logs now include one empty line after the actual message and the closing asterisks

## [1.1.0] - 2021-04-07

### Added

- block logs
- different dump stack behavior

## [1.0.0] - 2021-04-06

### Added

- the Logger class `L`
- basic methods for all log levels
